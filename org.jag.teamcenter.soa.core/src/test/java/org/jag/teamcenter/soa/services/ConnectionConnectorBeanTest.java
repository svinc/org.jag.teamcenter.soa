/*
 * MIT License
 *
 * Copyright (c) 2018 José A. García Sánchez
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package org.jag.teamcenter.soa.services;

import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.argThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.jag.teamcenter.soa.model.ConnectionConfiguration;
import org.jag.teamcenter.soa.model.Credentials;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.teamcenter.schemas.soa._2006_03.exceptions.InvalidCredentialsException;
import com.teamcenter.schemas.soa._2006_03.exceptions.ServiceException;
import com.teamcenter.services.loose.core.SessionService;
import com.teamcenter.soa.exceptions.CanceledOperationException;

/**
 * @author José A. García Sánchez
 */
public class ConnectionConnectorBeanTest {

    @InjectMocks
    private ConnectionConnectorBean underTest;

    @Mock
    private ConnectionPoolBean connectionPoolMock;

    @Mock
    private SessionServiceProviderBean sessionServiceProviderMock;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void connectHttp() throws CanceledOperationException {
        final CredentialsBean credentials = new CredentialsBean();
        credentials.setUsername("infodba");
        credentials.setPassword("infodba");
        credentials.setGroup("group");
        credentials.setRole("role");
        final ConnectionConfiguration connectionConfig = new ConnectionConfigurationHttpBean(
                "http://localhost:8080/tc/", "SoaAppX");

        underTest.connect(connectionConfig, credentials);

        verify(connectionPoolMock).setConnectionBean(argThat(new BaseMatcher<ConnectionBean>() {
            @Override
            public boolean matches(final Object item) {
                final ConnectionBean actual = (ConnectionBean) item;
                final Credentials actualCredentials = actual.getCredentials();

                return "infodba".equals(actualCredentials.getUsername())
                        && "infodba".equals(actualCredentials.getPassword())
                        && "group".equals(actualCredentials.getGroup()) && "role".equals(actualCredentials.getRole())
                        && "SoaAppX".equals(actual.getDiscriminator());
            }

            @Override
            public void describeTo(final Description description) {
            }
        }));
    }

    @Test
    public void login() throws InvalidCredentialsException, SessionLoginException {
        final SessionService sessionService = mock(SessionService.class);
        when(sessionServiceProviderMock.getService()).thenReturn(sessionService);

        final CredentialsBean credentials = new CredentialsBean();
        credentials.setUsername("username");
        credentials.setPassword("password");
        credentials.setGroup("group");
        credentials.setRole("role");
        final ConnectionBean connectionBean = new ConnectionBean();
        connectionBean.setCredentials(credentials);
        connectionBean.setDiscriminator("SoaAppX");

        when(connectionPoolMock.getConnectionBean()).thenReturn(connectionBean);

        underTest.login();

        verify(sessionService).login("username", "password", "group", "role", "", "SoaAppX");
    }

    @Test(expected = SessionLoginException.class)
    public void loginWithException() throws InvalidCredentialsException, SessionLoginException {
        final SessionService sessionService = mock(SessionService.class);
        when(sessionServiceProviderMock.getService()).thenReturn(sessionService);

        final CredentialsBean credentials = new CredentialsBean();
        credentials.setUsername("username");
        credentials.setPassword("password");
        credentials.setGroup("group");
        credentials.setRole("role");
        final ConnectionBean connectionBean = new ConnectionBean();
        connectionBean.setCredentials(credentials);
        connectionBean.setDiscriminator("SoaAppX");

        when(connectionPoolMock.getConnectionBean()).thenReturn(connectionBean);
        when(sessionService.login(anyString(), anyString(), anyString(), anyString(), anyString(), anyString()))
                .thenThrow(new InvalidCredentialsException("Fake exception"));

        underTest.login();

        verify(sessionService).login("username", "password", "group", "role", "", "SoaAppX");
    }

    @Test
    public void logout() throws ServiceException {
        final SessionService sessionService = mock(SessionService.class);
        when(sessionServiceProviderMock.getService()).thenReturn(sessionService);

        underTest.logout();

        verify(sessionService).logout();
    }

    @Test
    public void logoutWithException() throws ServiceException {
        final SessionService sessionService = mock(SessionService.class);
        when(sessionService.logout()).thenThrow(new ServiceException("Fake exception"));
        when(sessionServiceProviderMock.getService()).thenReturn(sessionService);

        underTest.logout();

        verify(sessionService).logout();
    }
}
