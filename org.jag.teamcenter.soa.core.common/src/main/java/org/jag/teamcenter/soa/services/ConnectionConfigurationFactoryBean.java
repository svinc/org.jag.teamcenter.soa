/*
 * MIT License
 *
 * Copyright (c) 2018 José A. García Sánchez
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package org.jag.teamcenter.soa.services;

import org.jag.teamcenter.soa.model.ConnectionConfiguration;
import org.jag.teamcenter.soa.model.Protocol;

/**
 * @author José A. García Sánchez
 */
class ConnectionConfigurationFactoryBean implements ConnectionConfigurationFactory {

    @Override
    public ConnectionConfiguration createConnectionConfiguration(final String host, final String discriminator) {
        final Protocol protocol = getProtocol(host);
        if (protocol == null) {
            return null;
        }

        ConnectionConfiguration connectionConfiguration = null;
        switch (protocol) {
        case HTTP:
            connectionConfiguration = new ConnectionConfigurationHttpBean(host, discriminator);
            break;

        case IIOP:
            connectionConfiguration = new ConnectionConfigurationIiopBean(host, discriminator);
            break;

        case TCCS:
            connectionConfiguration = new ConnectionConfigurationTccsBean(host, discriminator);
            break;

        default:
            break;
        }

        return connectionConfiguration;
    }

    private Protocol getProtocol(final String host) {
        final Protocol protocol;
        if (host.startsWith("http")) {
            protocol = Protocol.HTTP;
        } else if (host.startsWith("tccs")) {
            protocol = Protocol.TCCS;
        } else if (host.startsWith("iiop")) {
            protocol = Protocol.IIOP;
        } else {
            protocol = null;
        }

        return protocol;
    }
}
