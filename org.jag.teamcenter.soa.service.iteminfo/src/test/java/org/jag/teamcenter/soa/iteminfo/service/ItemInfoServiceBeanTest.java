/*
 * MIT License
 *
 * Copyright (c) 2018 José A. García Sánchez
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package org.jag.teamcenter.soa.iteminfo.service;

import static com.google.common.truth.Truth.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.jag.teamcenter.soa.iteminfo.model.ItemInfo;
import org.jag.teamcenter.soa.services.GroupBean;
import org.jag.teamcenter.soa.services.GroupConnector;
import org.jag.teamcenter.soa.services.ItemBean;
import org.jag.teamcenter.soa.services.ItemConnector;
import org.jag.teamcenter.soa.services.UserBean;
import org.jag.teamcenter.soa.services.UserConnector;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

/**
 * @author José A. García Sánchez
 */
public class ItemInfoServiceBeanTest {
    @InjectMocks
    private ItemInfoServiceBean underTest;

    @Mock
    private ItemConnector itemConnectorMock;

    @Mock
    private UserConnector userConnectorMock;

    @Mock
    private GroupConnector groupConnectorMock;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void find() {
        final ItemBean item = mock(ItemBean.class);
        when(item.getId()).thenReturn("012345");
        when(item.getName()).thenReturn("Item name");
        when(item.getType()).thenReturn("Excel template");
        when(item.getNumRevisions()).thenReturn(2);
        when(itemConnectorMock.findItemById(anyString())).thenReturn(item);

        final UserBean user = mock(UserBean.class);
        when(user.getId()).thenReturn("user id #1");
        when(user.getName()).thenReturn("user name");
        when(userConnectorMock.getOwningUser(any(ItemBean.class))).thenReturn(user);

        final GroupBean group = mock(GroupBean.class);
        when(group.getName()).thenReturn("group name");
        when(groupConnectorMock.getOwningGroup(any(ItemBean.class))).thenReturn(group);

        final ItemInfo itemInfo = underTest.find("012345");

        assertThat(itemInfo).isNotNull();
        assertThat(itemInfo.getId()).isEqualTo("012345");
        assertThat(itemInfo.getName()).isEqualTo("Item name");
        assertThat(itemInfo.getType()).isEqualTo("Excel template");
        assertThat(itemInfo.getNumRevisions()).isEqualTo(2);
        assertThat(itemInfo.getUser().getId()).isEqualTo("user id #1");
        assertThat(itemInfo.getUser().getName()).isEqualTo("user name");
        assertThat(itemInfo.getGroup().getName()).isEqualTo("group name");

        verify(itemConnectorMock).findItemById(eq("012345"));
        verify(userConnectorMock).getOwningUser(eq(item));
        verify(groupConnectorMock).getOwningGroup(eq(item));
    }

    @Test
    public void findItemNotFound() {
        when(itemConnectorMock.findItemById(anyString())).thenReturn(null);

        final ItemInfo itemInfo = underTest.find("012345");

        assertThat(itemInfo).isNull();
    }

    @Test
    public void findItemWithoutGroup() {
        final ItemBean item = mock(ItemBean.class);
        when(item.getId()).thenReturn("012345");
        when(item.getName()).thenReturn("Item name");
        when(item.getType()).thenReturn("Excel template");
        when(item.getNumRevisions()).thenReturn(2);
        when(itemConnectorMock.findItemById(anyString())).thenReturn(item);

        final UserBean user = mock(UserBean.class);
        when(user.getId()).thenReturn("user id #1");
        when(user.getName()).thenReturn("user name");
        when(userConnectorMock.getOwningUser(any(ItemBean.class))).thenReturn(user);

        when(groupConnectorMock.getOwningGroup(any(ItemBean.class))).thenReturn(null);

        final ItemInfo itemInfo = underTest.find("012345");

        assertThat(itemInfo).isNotNull();
        assertThat(itemInfo.getId()).isEqualTo("012345");
        assertThat(itemInfo.getName()).isEqualTo("Item name");
        assertThat(itemInfo.getType()).isEqualTo("Excel template");
        assertThat(itemInfo.getNumRevisions()).isEqualTo(2);
        assertThat(itemInfo.getUser().getId()).isEqualTo("user id #1");
        assertThat(itemInfo.getUser().getName()).isEqualTo("user name");
        assertThat(itemInfo.getGroup()).isNotNull();
        assertThat(itemInfo.getGroup().getName()).isEmpty();

        verify(itemConnectorMock).findItemById(eq("012345"));
        verify(userConnectorMock).getOwningUser(eq(item));
        verify(groupConnectorMock).getOwningGroup(eq(item));
    }
}
