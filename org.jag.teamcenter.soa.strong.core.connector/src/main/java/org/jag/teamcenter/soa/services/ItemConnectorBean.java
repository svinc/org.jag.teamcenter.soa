/*
 * MIT License
 *
 * Copyright (c) 2018 José A. García Sánchez
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package org.jag.teamcenter.soa.services;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;
import com.teamcenter.services.strong.core.DataManagementService;
import com.teamcenter.services.strong.core._2007_01.DataManagement;
import com.teamcenter.services.strong.core._2007_01.DataManagement.GetItemFromIdPref;
import com.teamcenter.services.strong.core._2009_10.DataManagement.GetItemFromAttributeInfo;
import com.teamcenter.services.strong.core._2009_10.DataManagement.GetItemFromAttributeResponse;
import com.teamcenter.soa.client.model.ModelObject;
import com.teamcenter.soa.client.model.strong.Item;
import com.teamcenter.soa.exceptions.NotLoadedException;

/**
 * @author José A. García Sánchez
 */
final class ItemConnectorBean implements ItemConnector {
    private static final Logger LOGGER = LoggerFactory.getLogger(ItemConnectorBean.class);

    private final DataManagementServiceProviderBean dataManagementServiceProvider;

    @Inject
    ItemConnectorBean(final DataManagementServiceProviderBean dataManagementService) {
        this.dataManagementServiceProvider = dataManagementService;
    }

    @Override
    public ItemBeanImpl findItemById(final String itemId) {

        final DataManagementService service = dataManagementServiceProvider.getService();
        final Item tcItem = tcFindItemById(itemId);
        if (tcItem == null) {
            return null;
        }
        service.getProperties(new ModelObject[] { tcItem },
                new String[] { "object_name", "object_type", "revision_list" });

        final ItemBeanImpl item = new ItemBeanImpl();
        item.setItem(tcItem);
        item.setId(itemId);

        try {
            item.setName(tcItem.get_object_name());
        } catch (NotLoadedException e) {
            LOGGER.warn("Error loading property item.[{}]", "object_name");
            item.setName("");
        }
        try {
            item.setType(tcItem.get_object_type());
        } catch (NotLoadedException e) {
            LOGGER.warn("Error loading property item.[{}]", "object_type");
            item.setType("");
        }
        try {
            item.setNumRevisions(tcItem.get_revision_list().length);
        } catch (NotLoadedException e) {
            LOGGER.warn("Error loading property item.[{}]", "revision_list");
            item.setNumRevisions(-1);
        }

        return item;
    }

    public Item tcFindItemById(final String itemId) {
        final Map<String, Object> itemAttributes = new HashMap<>();
        itemAttributes.put("item_id", itemId);
        final GetItemFromAttributeInfo[] infos = new GetItemFromAttributeInfo[1];
        infos[0] = new GetItemFromAttributeInfo();
        infos[0].itemAttributes = itemAttributes;
        infos[0].revIds = new String[0];

        int arg1 = 1;
        final GetItemFromIdPref pref = new GetItemFromIdPref();
        pref.prefs = new DataManagement.RelationFilter[0];

        final DataManagementService service = dataManagementServiceProvider.getService();

        final GetItemFromAttributeResponse response = service.getItemFromAttribute(infos, arg1, pref);
        if (response.output.length == 0) {
            return null;
        }
        return response.output.clone()[0].item;
    }
}
