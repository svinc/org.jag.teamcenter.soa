/*
 * (c) 2018 - Jose Alberto Garcia Sanchez 
 */
package org.jag.hamcrest.matchers;

import org.hamcrest.Matcher;

import com.teamcenter.services.strong.core._2009_10.DataManagement.GetItemFromAttributeInfo;

/**
 * @author Jose A. Garcia Sanchez
 */
public class GetItemFromAttributeInfoMatcher {

    public static Matcher<GetItemFromAttributeInfo[]> arrayHasSize(int expected) {
        return new ArrayHasSize<>(expected);
    }
}
